#include "Enemy.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <stdlib.h>
#include "Game.h"

extern Game *game;

Enemy::Enemy(): QObject(), QGraphicsPixmapItem()
{
    int randXPosEnemy = rand() % 440;
    setPos(960, randXPosEnemy);

    //setRect(0,0,100,70);
    setPixmap(QPixmap(":images/SharkGameFin.png"));
    setScale(0.8);
    //setBrush(QBrush(QColor(0, 150, 0)));

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()),this,SLOT(move()));

    timer->start(50);
}

void Enemy::move()
{
    QList<QGraphicsItem *> collision = collidingItems();
    for (int i = 0, n = collision.size(); i < n; ++i)
    {
        if (typeid(*(collision[i])) == typeid(Player)){
            game->healthPoints->decrease();
            scene()->removeItem(this);
            delete this;
            return;
        }
    }

    setPos(x()-5, y());

    if (pos().x() < 50){
        game->healthPoints->decrease();
        scene()->removeItem(this);
        delete this;
        //qDebug() << "gone";
    }
}
