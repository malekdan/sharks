#include "Player.h"
#include <QKeyEvent>
#include <QDebug>
#include "Projectile.h"
#include <QGraphicsScene>
#include "Enemy.h"
#include <QObject>

Player::Player(): QObject(), QGraphicsPixmapItem()
{

    setPixmap(QPixmap(":images/SharkGameBoat.png"));
    setScale(1.3);
    //setBrush(QBrush(QColor(0, 0, 200)));
}

void Player::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "Key Pressed";

    if ((event->key() == Qt::Key_Left)||(event->key() == Qt::Key_A)){
        if (pos().x() > 50){
        setPos(x () -10, y()) ;
        }
        }
    else if ((event->key() == Qt:: Key_Right)||(event->key() == Qt::Key_D)) {
        if (pos().x() < 860){
        setPos(x () +10, y()) ;
        }
    }
    else if ((event->key() == Qt:: Key_Up)||(event->key() == Qt::Key_W)){
            if (pos().y() > 0){
        setPos(x(), y() -10) ;
            }
    }
    else if ((event->key() == Qt:: Key_Down)||(event->key() == Qt::Key_S)){
        if (pos().y() < 440){
        setPos(x(), y() +10) ;
        }
        }
    else if (event->key() == Qt::Key_Space){
        Projectile *projectile = new Projectile();
        //qDebug() << "shoot shoot";
        projectile->setPos(x(), y());
        scene()->addItem(projectile);
    }

}

void Player::spawn()
{
    Enemy *enemy = new Enemy();
    scene()->addItem(enemy);
}
