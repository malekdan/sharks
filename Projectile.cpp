#include "Projectile.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <QList>
#include "Enemy.h"
#include "Game.h"

extern Game *game;

Projectile::Projectile(QGraphicsItem *parent)
    : QObject(), QGraphicsEllipseItem(parent) {
    setRect(105, 40, 10, 10);
    setBrush(QBrush(QColor(0, 0, 0)));

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));

    timer->start(50);
}

void Projectile::move()
{
    QList<QGraphicsItem *> collision = collidingItems();
    for (int i = 0, n = collision.size(); i < n; ++i)
    {
        if (typeid(*(collision[i])) == typeid(Enemy)){
            game->score->increase();
            scene()->removeItem(collision[i]);
            scene()->removeItem(this);
            delete collision[i];
            delete this;
            return;
        }
    }


    setPos(x()+12, y());

    if (pos().x() > 960){
        scene()->removeItem(this);
        delete this;
        //qDebug() << "gone";
    }
}
