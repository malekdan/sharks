#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QPushButton>
#include "Player.h"
#include "Text.h"

class Game : public QGraphicsView {
    Q_OBJECT
public:
    Game(QWidget *parent=0);

    QGraphicsScene *mainMenuScene;
    QGraphicsScene *tutorialScene;
    QGraphicsScene *gameScene;
    QGraphicsScene *gameOverScene;
    Player *player;
    Score *score;
    Health *healthPoints;

public slots:
    void tutorialScreen();
    void startGame();
    void gameOverScreen();
};

#endif // GAME_H
