#ifndef TEXT_H
#define TEXT_H

#include <QGraphicsTextItem>
#include <QObject>

class Score: public QGraphicsTextItem {
public:
    Score(QGraphicsItem *parent=0);
    void increase();
    int getScore();
private:
    int score;
};

class Health: public QGraphicsTextItem {
    Q_OBJECT
public:
    Health(QGraphicsItem *parent=0);
    void decrease();
    int getHealth();
signals:
    void healthDepleted();
private:
    int healthPoints;
};

#endif // TEXT_H
