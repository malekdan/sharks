#include <QGraphicsScene>
#include "Player.h"
#include <QGraphicsView>
#include <QTimer>
#include <QPushButton>
#include <QGraphicsTextItem>
#include <QFont>
#include <QBrush>
#include <QImage>
#include "Game.h"

Game::Game(QWidget *parent) : QGraphicsView(parent) {
    // Main menu scene setup
    mainMenuScene = new QGraphicsScene();
    mainMenuScene->setSceneRect(0, 0, 960, 540);
    mainMenuScene->setBackgroundBrush(Qt::black);
    setScene(mainMenuScene);

    // Pictures
    QGraphicsPixmapItem *mainMenuImage = new QGraphicsPixmapItem(QPixmap(":/images/SharkGameBoat.png"));
    mainMenuImage->setPos(490, 300);
    mainMenuImage->setScale(2.0);
    mainMenuScene->addItem(mainMenuImage);
    QGraphicsPixmapItem *mainMenuImageFin = new QGraphicsPixmapItem(QPixmap(":/images/SharkGameFin.png"));
    mainMenuImageFin->setPos(800, 280);
    mainMenuImageFin->setScale(0.8);
    mainMenuScene->addItem(mainMenuImageFin);


    // Game title text
    QGraphicsTextItem *titleText = new QGraphicsTextItem(QString("Sharks?"));
    titleText->setDefaultTextColor(Qt::white);
    QFont titleFont("Verdana", 100);
    titleText->setFont(titleFont);
    int txPos = this->width() / 2 - titleText->boundingRect().width() / 2;
    titleText->setPos(txPos, 120);
    mainMenuScene->addItem(titleText);

    // Play button
    QPushButton *playButton = new QPushButton("Play");
    int bxPos = this->width() / 2 - playButton->width() / 2;
    playButton->setGeometry(bxPos, 300, 200, 50);
    mainMenuScene->addWidget(playButton);

    connect(playButton, &QPushButton::clicked, this, &Game::tutorialScreen);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(960, 540);

    show();
}
void Game::tutorialScreen() {
    // Main menu scene setup
    tutorialScene = new QGraphicsScene();
    tutorialScene->setSceneRect(0, 0, 960, 540);
    tutorialScene->setBackgroundBrush(Qt::black);
    setScene(tutorialScene);

    // Instructions
    QGraphicsPixmapItem *spaceBarImage = new QGraphicsPixmapItem(QPixmap(":/images/spacebar.png"));
    spaceBarImage->setPos(220, 250);
    spaceBarImage->setScale(0.5);
    tutorialScene->addItem(spaceBarImage);
    QGraphicsPixmapItem *keysImage = new QGraphicsPixmapItem(QPixmap(":/images/keyArrows.png"));
    keysImage->setPos(570, 200);
    keysImage->setScale(0.4);
    tutorialScene->addItem(keysImage);


    QFont instructionsFont("Arial", 27);
    QGraphicsTextItem *spaceText = new QGraphicsTextItem(QString("Press SPACE to shoot"));
    spaceText->setDefaultTextColor(Qt::white);
    spaceText->setFont(instructionsFont);
    spaceText->setPos(160,350);
    tutorialScene->addItem(spaceText);

    QGraphicsTextItem *arrowsText = new QGraphicsTextItem(QString("Use ARROWS to move"));
    arrowsText->setDefaultTextColor(Qt::white);
    arrowsText->setFont(instructionsFont);
    arrowsText->setPos(560,350);
    tutorialScene->addItem(arrowsText);


    // Game title text
    QGraphicsTextItem *tutorialText = new QGraphicsTextItem(QString("How to play"));
    tutorialText->setDefaultTextColor(Qt::white);
    QFont titleFont("Arial", 45);
    tutorialText->setFont(titleFont);
    tutorialText->setPos(tutorialScene->width()/2- tutorialText->boundingRect().width() / 2, 80);
    tutorialScene->addItem(tutorialText);

    // Play button
    QPushButton *playButton = new QPushButton("Start");
    playButton->setGeometry(tutorialScene->width() / 2 - 100,400, 200, 50);
    tutorialScene->addWidget(playButton);

    connect(playButton, &QPushButton::clicked, this, &Game::startGame);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(960, 540);

    show();
}

void Game::startGame() {

    if (gameScene) {
        gameScene->clear();
    }

    // Game scene setup
    gameScene = new QGraphicsScene();
    gameScene->setSceneRect(0, 0, 960, 540);
    setBackgroundBrush(QBrush(QImage(":/images/SharkGameBackground.png")));

    setScene(gameScene);

    // Everything about the player
    player = new Player();
    player->setPos(100, 230);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    gameScene->addItem(player);

    // Score and HP
    score = new Score();
    score->setPos(gameScene->width() - 120, 0);
    gameScene->addItem(score);

    healthPoints = new Health();
    healthPoints->setPos(60, 0);
    gameScene->addItem(healthPoints);

    connect(healthPoints, &Health::healthDepleted, this, &Game::gameOverScreen);

    // Spawning enemies on the right side of the window randomly
    QTimer *timer = new QTimer();
    QObject::connect(timer, SIGNAL(timeout()), player, SLOT(spawn()));
    timer->start(2000);

    show();
}

void Game::gameOverScreen() {
    // Game over scene setup
    gameOverScene = new QGraphicsScene();
    gameOverScene->setSceneRect(0, 0, 960, 540);
    setBackgroundBrush(QBrush(Qt::black));
    //gameOverScene->setBackgroundBrush(Qt::black);
    setScene(gameOverScene);

    // Game title text
    QGraphicsTextItem *gameOverText = new QGraphicsTextItem(QString("Game Over"));
    gameOverText->setDefaultTextColor(Qt::white);
    QFont titleFont("Arial", 45);
    gameOverText->setFont(titleFont);
    gameOverText->setPos(tutorialScene->width()/2- gameOverText->boundingRect().width() / 2, 80);
    gameOverScene->addItem(gameOverText);

    // Final score text
    QGraphicsTextItem *finalScoreText = new QGraphicsTextItem(QString("Score: ") + QString::number(score->getScore()));
    QFont scoreFont("Arial", 30);
    finalScoreText->setFont(scoreFont);
    finalScoreText->setPos(gameOverScene->width() / 2 - finalScoreText->boundingRect().width() / 2, 200);
    gameOverScene->addItem(finalScoreText);

    // Restart button
    QPushButton *playButton = new QPushButton("Restart");
    playButton->setGeometry(tutorialScene->width() / 2 - 100,400, 200, 50);
    gameOverScene->addWidget(playButton);

    connect(playButton, &QPushButton::clicked, this, &Game::startGame);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(960, 540);

    show();
}
