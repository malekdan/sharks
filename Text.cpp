#include "Text.h"
#include <QFont>

Score::Score(QGraphicsItem *parent) : QGraphicsTextItem(parent) {
    score = 0;
    setPlainText(QString("Score: ") + QString::number(score));
    setDefaultTextColor(Qt::white);
    QFont font("Verdana", 20);
    setFont(font);
}

void Score::increase() {
    score++;
    setPlainText(QString("Score: ") + QString::number(score));
}

int Score::getScore() {
    return score;
}

Health::Health(QGraphicsItem *parent) : QGraphicsTextItem(parent) {
    healthPoints = 3;
    setPlainText(QString("Lives: ") + QString::number(healthPoints));
    setDefaultTextColor(Qt::red);
    QFont font("Verdana", 20);
    setFont(font);
}

void Health::decrease() {
    healthPoints--;
    setPlainText(QString("Lives: ") + QString::number(healthPoints));
    if (healthPoints == 0) {
        emit healthDepleted();
    }
}

int Health::getHealth() {
    return healthPoints;
}
