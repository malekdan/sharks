#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <QGraphicsEllipseItem>
#include <QGraphicsItem>
#include <QObject>

class Projectile: public QObject, public QGraphicsEllipseItem{
    Q_OBJECT
public:
    Projectile(QGraphicsItem *parent=0);
public slots:
    void move();

};

#endif // PROJECTILE_H
